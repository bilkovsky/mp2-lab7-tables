// TSortTable.cpp
// ������������� �������

#include "include/TSortTable.h"

TSortTable & TSortTable::operator=(const TScanTable &tab)
{
    if (pRecs != nullptr) {
        for (int i = 0; i < DataCount; ++i)
            delete pRecs[i];
        delete[] pRecs;
        pRecs = nullptr;
    }
    TabSize = tab.TabSize;
    DataCount = tab.DataCount;
    pRecs = new PTTabRecord[TabSize];
    for (int i = 0; i < DataCount; ++i)
        pRecs[i] = (PTTabRecord)tab.pRecs[i]->GetCopy();
    SortData();
    CurrPos = 0;
    return *this;
}

        /*-------------------------------------------*/

PTDatValue TSortTable::FindRecord(TKey k)
{
    SetRetCode(TabNoRec);
    PTDatValue result = nullptr;
    if (DataCount > 0) {
        int i, i1 = 0, i2 = DataCount - 1;
        while (i1 <= i2) {
            i = (i1 + i2) / 2;
            if (pRecs[i]->GetKey() == k) {
                result = pRecs[i]->GetValuePtr();
                SetRetCode(TabOK);
                break;
            }
            else if (pRecs[i]->GetKey() > k) {
                i2 = i - 1;
            }
            else {
                i1 = i + 1;
            }
        }
    }
    return result;
}

        /*-------------------------------------------*/

void TSortTable::InsRecord(TKey k, PTDatValue pVal)
{
    if (IsFull()) {
        SetRetCode(TabFull);
    }
    else {
        if (FindRecord(k) != nullptr) {
            SetRetCode(TabRecDbl);
        }
        else {
            SetRetCode(TabOK);
            for (int i = DataCount; i > CurrPos; i--)
                pRecs[i] = pRecs[i - 1];
            pRecs[CurrPos] = new TTabRecord(k, pVal);
            DataCount++;
            SortData();
        }
    }
}

        /*-------------------------------------------*/

void TSortTable::DelRecord(TKey k)
{
    SetRetCode(TabNoRec);
    for (int i = 0; i < DataCount; i++) {
        if (pRecs[i]->GetKey() == k) {
            delete pRecs[i];
            for (int j = i; j < DataCount; j++)
                pRecs[j] = pRecs[j + 1];
            DataCount--;
            SetRetCode(TabOK);
        }
    }
}

        /*-------------------------------------------*/

TSortMethod TSortTable::GetSortMethod()
{
    return SortMethod;
}

        /*-------------------------------------------*/

void TSortTable::SetSortMethod(TSortMethod method)
{
    SortMethod = method;
}

        /*-------------------------------------------*/

void TSortTable::SortData()
{
    switch (SortMethod) {
    case TSortMethod::QUICK_SORT:
        QuickSort(pRecs, DataCount);
        break;
    case TSortMethod::INSERT_SORT:
        InsertSort(pRecs, DataCount);
        break;
    case TSortMethod::MERGE_SORT:
        QuickSort(pRecs, DataCount);
        break;
    default:
        InsertSort(pRecs, DataCount);
        break;
    }
}

        /*-------------------------------------------*/

void TSortTable::InsertSort(PTTabRecord *pMem, int DataCount)
{
    PTTabRecord pR;
    for (int i = 1, j; i < DataCount; i++)
    {
        pR = pRecs[i];
        for (j = i - 1; j >= 0; j--) {
            if (pRecs[j]->GetKey() > pR->GetKey()) {
                pRecs[j + 1] = pRecs[j];
            }
            else {
                break;
            }
        }
        pRecs[j + 1] = pR;
    }
}

        /*-------------------------------------------*/

void TSortTable::MergeSort(PTTabRecord * pMem, int DataCount)
{
    PTTabRecord *pData = pRecs;
    PTTabRecord *pBuff = new PTTabRecord[DataCount];
    PTTabRecord *pTemp = pBuff;
    MergeSorter(pData, pBuff, DataCount);
    if (pData == pTemp) {
        for (int i = 0; i < DataCount; i++)
            pBuff[i] = pData[i];
    }
    delete pTemp;
}

        /*-------------------------------------------*/

void TSortTable::MergeSorter(PTTabRecord *& pData, PTTabRecord *& pBuff, int Size)
{
    int n1 = (Size + 1) / 2;
    int n2 = Size - n1;
    if (Size > 2) {
        PTTabRecord *pDat2 = pData + n1, *pBuff2 = pBuff + n1;
        MergeSorter(pData, pBuff, n1);
        MergeSorter(pDat2, pBuff2, n2);
    }
    MergeData(pData, pBuff, n1, n2);
}

        /*-------------------------------------------*/

void TSortTable::MergeData(PTTabRecord *& pData, PTTabRecord *& pBuff, int n1, int n2)
{
    for (int i = 0; i < (n1 + n2); i++) {
        pBuff[i] = pData[i];
    }
    PTTabRecord *&tmp = pData;
    pData = pBuff;
    pBuff = tmp;
}

        /*-------------------------------------------*/

void TSortTable::QuickSort(PTTabRecord * pRecs, int DataCount)
{
    int pivot; // ������ �������� ��������
    int n1, n2; // ������� ����������� ������ ������
    if (DataCount > 1) {
        QuickSplit(pRecs, DataCount, pivot);
        n1 = pivot + 1;
        n2 = DataCount - n1;
        QuickSort(pRecs, n1 - 1);
        QuickSort(pRecs + n1, n2);
    }
}

        /*-------------------------------------------*/

void TSortTable::QuickSplit(PTTabRecord * pData, int Size, int &Pivot)
{
    PTTabRecord pPivot = pData[0], pTemp;
    int i1 = 1, i2 = Size - 1;
    while (i1 <= i2) {
        while ((i1 < Size) && !(pData[i1]->GetKey() > pPivot->GetKey())) i1++;
        while (pData[i2]->GetKey() > pPivot->GetKey()) i2--;
        if (i1 < i2) {
            pTemp = pData[i1];
            pData[i1] = pData[i2];
            pData[i2] = pTemp;
        }
    }
    pData[0] = pData[i2];
    pData[i2] = pPivot;
    Pivot = i2;
}
