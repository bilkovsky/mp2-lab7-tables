// TTabRecord.h
// ����� ��������-�������� ��� ������� �������

#include "include/TTabRecord.h"

TTabRecord::TTabRecord(TKey k, PTDatValue pVal)
{
    Key = k;
    pValue = pVal;
}

        /*-------------------------------------------*/

void TTabRecord::SetKey(TKey k) {
    Key = k;
}

        /*-------------------------------------------*/

TKey TTabRecord::GetKey() const {
    return Key;
}

        /*-------------------------------------------*/

void TTabRecord::SetValuePtr(PTDatValue p) {
    pValue = p;
}

        /*-------------------------------------------*/

PTDatValue TTabRecord::GetValuePtr() const {
    return pValue;
}

        /*-------------------------------------------*/

TDatValue* TTabRecord::GetCopy()
{
    TDatValue* temp = new TTabRecord(Key, pValue);
    return temp;
}

        /*-------------------------------------------*/

TTabRecord& TTabRecord::operator=(TTabRecord& tr)
{
    Key = tr.Key;
    pValue = tr.pValue;
    return *this;
}

        /*-------------------------------------------*/

bool TTabRecord::operator==(const TTabRecord& tr)
{
    return Key == tr.Key;
}

    /*-------------------------------------------*/

bool TTabRecord::operator<(const TTabRecord& tr)
{
    return Key < tr.Key;
}

    /*-------------------------------------------*/

bool TTabRecord::operator>(const TTabRecord& tr)
{
    return Key > tr.Key;
}
