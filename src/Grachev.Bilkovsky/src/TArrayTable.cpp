// TArrayTable.cpp
// ������� - ������� ����� ��� ������ � ����������� �������

#include "include/TArrayTable.h"

TArrayTable::TArrayTable(int size) {
    pRecs = new PTTabRecord[size];
    for (int i = 0; i < size; ++i) pRecs[i] = nullptr;
    TabSize = size;
    DataCount = CurrPos = 0;
}

        /*-------------------------------------------*/

TArrayTable::~TArrayTable()
{
    for (int i = 0; i < DataCount; ++i)
        delete pRecs[i];
    delete[] pRecs;
}

        /*-------------------------------------------*/

TKey TArrayTable::GetKey(TDataPos mode) const
{
    int pos = -1;
    switch (mode)
    {
    case TDataPos::FIRST_POS:
        pos = 0;
        break;
    case TDataPos::LAST_POS:
        pos = DataCount - 1;
        break;
    case TDataPos::CURRENT_POS:
        pos = CurrPos;
        break;
    }
    return (pos != -1) ? pRecs[pos]->Key : "";
}

        /*-------------------------------------------*/

PTDatValue TArrayTable::GetValuePtr(TDataPos mode) const
{
    int pos = -1;
    switch (mode)
    {
    case TDataPos::FIRST_POS:
        pos = 0;
        break;
    case TDataPos::LAST_POS:
        pos = DataCount - 1;
        break;
    case TDataPos::CURRENT_POS:
        pos = CurrPos;
        break;
    }
    return (pos != -1) ? pRecs[pos]->pValue : nullptr;
}

        /*-------------------------------------------*/

PTTabRecord TArrayTable::GetCurrRecord() {
    return pRecs[CurrPos];
}

        /*-------------------------------------------*/

void TArrayTable::Reset() {
    CurrPos = 0;
}

        /*-------------------------------------------*/

bool TArrayTable::IsTabEnded() const
{
    return CurrPos >= DataCount;
}

        /*-------------------------------------------*/

int TArrayTable::GoNext()
{
    if (!IsTabEnded())
        CurrPos++;
    else
        SetRetCode(TabFull);
    return GetRetCode();
}

        /*-------------------------------------------*/

int TArrayTable::SetCurrentPos(int pos)
{
    if (pos < 0 || pos >= DataCount)
        SetRetCode(TabNoRec);
    else
        CurrPos = pos;
    return GetRetCode();
}
