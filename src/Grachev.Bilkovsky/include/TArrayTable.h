// TArrayTable.h
// ������� - ������� ����� ��� ������ � ����������� �������

#ifndef _TARRAYTABLE_H_
#define _TARRAYTABLE_H_

#include "include/TTable.h"
#include "include/TTabRecord.h"

enum TDataPos { FIRST_POS, CURRENT_POS, LAST_POS };

class TArrayTable : public TTable
{
protected:
    PTTabRecord *pRecs;				// ������ ��� ������� �������
    int TabSize;					// ����. ����� ������� � �������
    int CurrPos;					// ����� ������� ������

public:
    static const int TAB_MAX_SIZE = 25;
    TArrayTable(int size = TAB_MAX_SIZE);
    ~TArrayTable();

    // �������������� ������
    virtual bool IsFull() const { return DataCount >= TabSize; } // ������� ���������?
    int GetTabSize() const { return TabSize; }

    // ������
    virtual TKey GetKey() const { return GetKey(CURRENT_POS); }
    virtual PTDatValue GetValuePtr() const { return GetValuePtr(CURRENT_POS); }
    virtual TKey GetKey(TDataPos mode) const;
    virtual PTDatValue GetValuePtr(TDataPos mode) const;

    // ����������� ���� �����, ����������� ��� ����������� ������� � ������, ��� ����������� ���������
    PTTabRecord GetCurrRecord();

    // ���������
    virtual void Reset();
    virtual bool IsTabEnded() const;
    virtual int GoNext();
    virtual int SetCurrentPos(int pos);
    int GetCurrentPos() const { return CurrPos; }

    friend TSortTable;
};

#endif
