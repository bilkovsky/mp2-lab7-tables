// TBalanceNode.h
// ������� - ������� ����� ��������-�������� ��� ���������������� ��������

#ifndef _BALANCENODE_H_
#define _BALANCENODE_H_

#include "include/TTreeNode.h"

enum class Bal { BalOK, BalLeft, BalRight };

class  TBalanceNode : public TTreeNode {
protected:
    Bal Balance; // ������ ������������ �������
public:
    TBalanceNode(TKey k = "", PTDatValue pVal = NULL, PTTreeNode pL = NULL,
        PTTreeNode pR = NULL, Bal bal = Bal::BalOK) : TTreeNode(k, pVal, pL, pR),
        Balance(bal) {};			// �����������
    virtual TDatValue * GetCopy();  // ���������� �����
    Bal GetBalance(void) const;
    void SetBalance(Bal bal);
    friend class TBalanceTree;
};

typedef TBalanceNode *PTBalanceNode;

#endif
