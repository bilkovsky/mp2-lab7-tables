// TSortTable.h
// ������������� �������

#ifndef _SORT_TABLE_H_
#define _SORT_TABLE_H_

#include "TScanTable.h"

enum TSortMethod { INSERT_SORT, MERGE_SORT, QUICK_SORT };

class TSortTable : public TScanTable
{
protected:
    TSortMethod SortMethod;
    void SortData();
    void InsertSort(PTTabRecord *pMem, int DataCount);
    void MergeSort(PTTabRecord *pMem, int DataCount);
    void MergeSorter(PTTabRecord* &pData, PTTabRecord* &pBuf, int Size);
    void MergeData(PTTabRecord* &pData, PTTabRecord* &pBuff, int n1, int n2);
    void QuickSort(PTTabRecord *pMem, int DataCount);
    void QuickSplit(PTTabRecord *pData, int Size, int &Pivot);

public:
    TSortTable(int Size) : TScanTable(Size) { };
    TSortTable & operator=(const TScanTable &tab);
    TSortMethod GetSortMethod();
    void SetSortMethod(TSortMethod method);

    // �������� ������
    virtual PTDatValue FindRecord(TKey k) override final;
    virtual void InsRecord(TKey k, PTDatValue pVal) override final;
    virtual void DelRecord(TKey k) override final;
};

#endif
