// TTabRecord.h
// ����� ��������-�������� ��� ������� �������

#ifndef _TREACORD_H_
#define _TREACORD_H_

#include <iostream>
#include <include/TDatValue.h>

using namespace std;

typedef string TKey;     // ��� ����� ������

class TTabRecord : public TDatValue {  
  protected:    
    TKey Key;   // ���� ������
    PTDatValue pValue;   // ��������� �� ��������
  public:
      TTabRecord(TKey k = "", PTDatValue pVal = NULL); // ����������� 
      void SetKey(TKey k); // ���������� �������� �����
      TKey GetKey(void) const;  // �������� �������� �����
      void SetValuePtr(PTDatValue p); // ���������� ��������� �� ������
      PTDatValue GetValuePtr(void) const; // �������� ��������� �� ������
      virtual TDatValue * GetCopy(); // ���������� �����
      TTabRecord& operator = (TTabRecord &tr); // ������������

      // ���������
      virtual bool operator == (const TTabRecord &tr);
      virtual bool operator < (const TTabRecord &tr);
      virtual bool operator > (const TTabRecord &tr);

      //������������� ������ ��� ��������� ����� ������, ��. �����
      friend class TArrayTable;
      friend class TScanTable;
      friend class TSortTable;
      friend class TTreeNode;
      friend class TTreeTable;
      friend class TArrayHash;
      friend class TListHash;
};

typedef TTabRecord* PTTabRecord;

#endif