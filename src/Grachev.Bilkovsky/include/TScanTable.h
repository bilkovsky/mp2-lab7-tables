// TScanTable.h
// ��������������� �������

#ifndef _TSCANTABLE_H_
#define _TSCANTABLE_H_

#include "include/TArrayTable.h"

class TScanTable : public TArrayTable
{
public:
    TScanTable(int Size) : TArrayTable(Size) { }

    // �������� ������
    virtual PTDatValue FindRecord(TKey k); // ����� ������
    virtual void InsRecord(TKey k, PTDatValue pVal); // �������� ������
    virtual void DelRecord(TKey k); // ������� ������
};

#endif
